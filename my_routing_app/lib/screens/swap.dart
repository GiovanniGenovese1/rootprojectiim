import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:my_routing_app/services/world_time.dart';

class Swap extends StatefulWidget {
  const Swap({super.key});

  @override
  State<Swap> createState() => _SwapState();
}

class _SwapState extends State<Swap> {
  List<WorldTime> locations = [
    WorldTime('Athens', 'Europe/Berlin'),
    WorldTime('London', 'Europe/London'),
    WorldTime('Chicago', 'America/Chicago'),
    WorldTime('Chicago', 'America/Chicago'),
    WorldTime('Chicago', 'America/Chicago'),
    WorldTime('Chicago', 'America/Chicago'),
    WorldTime('Chicago', 'America/Chicago'),
  ];

  void updateCountry(index) async {
    WorldTime instance = locations[index];
    await instance.getData();
    Navigator.pop(
        context, {'location': instance.location, 'time': instance.time});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.amber,
        appBar: AppBar(
          backgroundColor: Colors.grey,
          title: Text('Swap Country'),
        ),
        body: ListView.builder(
          itemBuilder: (context, index) {
            return (Padding(
              padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 4),
              child: Card(
                child: ListTile(
                  onTap: () {
                    updateCountry(index);
                  },
                  title: Text(locations[index].location),
                ),
              ),
            ));
          },
          itemCount: locations.length,
        ),
      ),
    );
  }
}
