import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data = {};
  @override
  Widget build(BuildContext context) {
    data = data.isNotEmpty
        ? data
        : ModalRoute.of(context)!.settings.arguments as Map;
    print(data);
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(0, 120, 0, 0),
        child: Column(children: <Widget>[
          TextButton(
            onPressed: () async {
              dynamic result = await Navigator.pushNamed(context, '/swap');
              setState(() {
                data = {'time': result['time'], 'location': result['location']};
              });
            },
            child: Text('Swap Country'),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                data['location'],
                style: TextStyle(fontSize: 26, letterSpacing: 2),
              ),
            ],
          ),
          SizedBox(height: 20),
          Text(
            data['time'],
            style: TextStyle(fontSize: 50),
          )
        ]),
      ),
    ));
  }
}
