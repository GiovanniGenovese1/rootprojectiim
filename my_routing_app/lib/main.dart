import 'package:flutter/material.dart';
import 'package:my_routing_app/screens/home.dart';
import 'package:my_routing_app/screens/loading.dart';
import 'package:my_routing_app/screens/swap.dart';

void main() {
  runApp(MaterialApp(
    // initialRoute: '/',
    routes: {
      '/': (context) => Loading(),
      '/home': (context) => Home(),
      '/swap': (context) => Swap(),
    },
  ));
}
